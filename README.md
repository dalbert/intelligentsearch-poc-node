# README #

### Install swagger module ###

Install using npm. For complete instructions, see the [install](./docs/install.md) page.

```bash
$ npm install -g swagger
```

### Start the project ###

```
#!bash

swagger project start
```