'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
var restify = require('restify');
var Q = require('Q');
var _ = require('lodash');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  getProducts: getProducts
};

function buildQuery(search){
  var searchString = createAndString(search.split(' '), true);
  //return encodeURI(searchString);
  return searchString;
}

function createAndString(terms, mandatory){
  return createTermsPredicate(terms, true);
}

function createTermsPredicate(terms, mandatory){
  var searchString = '';
  var termDelimiter = mandatory === true ? '%20AND%20' : ' OR ';


  //console.log('terms: ', terms);
  if (terms.length > 0){
    searchString += '"' + terms.shift() + '"';

    terms.forEach(function(term){
      if (term.length > 0){
        term = term.replace(/"/g, '\\\"');
        searchString += termDelimiter + '"' + term + '"';
      }
    });
    if (searchString.length > 0){
      searchString =  '(' + searchString + ')';
    }
  }
  return searchString;
}

function createOrString(terms, mandatory){
  return createTermsPredicate(terms, false);
}

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getProducts(req, res, next) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}

  //console.log('req.swagger.params: ', req.swagger.params);
  var myres = res;

  var results = {};
  var queryString = '/solr/products/select?indent=on&rows=300&wt=json&defType=dismax&qf=Product_Name';
  var client = restify.createJsonClient({
    url: 'http://localhost:8983',
    version: '*'
  });


  var productName = req.swagger.params.productName.value;
  var customerName = req.swagger.params.customerName.value;
  //console.log('customerName: ', customerName);

  if (!!productName){
    //console.log('productName: ', productName);
    queryString += '&q=' + buildQuery(productName);
  }else{
    queryString += '&q=*';
  }
  console.log('initial queryString: ', queryString);

  getBoostForOtherProducts(customerName)
    .then(function(boostQuery){

      queryString += encodeURI(boostQuery);
      //queryString += boostQuery;
      console.log('******* main queryString: ' + queryString);

      client.get(queryString, function(err, req, res, data) {
        if (err){
          next(new Error(err));

        }else{
          var docs = data.response.docs;
          results['docs'] = docs;
          myres.json(results);
        }
      });

    });

}


function findProductsBoughtByUsers(users){
  var defer = Q.defer();

  var client = restify.createJsonClient({
    url: 'http://localhost:8983',
    version: '*'
  });

  ///solr/orders/select?facet.field=userid&facet=on&indent=on&rows=10&wt=json&fl=Customer_Name&q=ProductId:' + createOrString(_.uniq(products)));
  var searchQuery = encodeURI('/solr/orders/select?facet.field=ProductId&facet=on&rows=10&wt=json&fl=ProductId,Product_Name&q=Customer_Name:' + createOrString(users));
  //var searchQuery = '/solr/orders/select?indent=on&rows=3000&wt=json&fl=Product_Name&q=Customer_Name:' + createOrString(users);
  var products = [];
  console.log('searchQuery: ' + searchQuery);
  client.get(searchQuery, function(err, req, res, data){
    if (err){
      console.log(err);
      //next(new Error(err));

    }else{
      var facetFields = data.facet_counts.facet_fields.ProductId;
      var count = 30;

      for(var i=0; i < facetFields.length && count > 0; i+=2){

        if (i+1 > facetFields.length){
          break;
        }
        if (facetFields[i+1] > 0){
          console.log('most bought product: adding: ' + facetFields[i] + ' -> ' + facetFields[i+1]);
          products.push({id: facetFields[i], count: facetFields[i+1]});
          count--;
        }else{
          break;
        }
      }
      //console.log('products: ', products);

    }
    defer.resolve(products)
  });


  return defer.promise;
}

function findUsersWhoBoughtProducts(products){
  var defer = Q.defer();
  var users = [];

  var client = restify.createJsonClient({
    url: 'http://localhost:8983',
    version: '*'
  });
  console.log('finding products: ', JSON.stringify(_.map(products, 'id')));
  var searchQuery = encodeURI('/solr/orders/select?facet.field=userid&facet=on&indent=on&rows=1000&wt=json&fl=Customer_Name&q=ProductId:' + createOrString(_.map(products, 'id')));
  //var searchQuery = '/solr/orders/select?facet.field=userid&facet=on&indent=on&rows=10&wt=json&fl=Customer_Name&q=Product_Name:' + createOrString(_.uniq(products));

  console.log('searchQuery: ' + searchQuery);
  //facet_counts
  client.get(searchQuery, function(err, req, res, data){
    if (err){
      console.log(err);

    }else{
      var facetFields = data.facet_counts.facet_fields.userid;
      var count = 30;
      //console.log('facetFields(' + facetFields.length + ') : ', facetFields);

      for(var i=0; i < facetFields.length && count > 0; i+=2){
        if (i+1 > facetFields.length){
          break;
        }
        if (facetFields[i+1] > 0){
          console.log('most common customers: adding: ' + facetFields[i] + ' -> ' + facetFields[i+1]);
          users.push(facetFields[i]);
          count--;
        }
      }
      //console.log('customers: ', users);

    }
    defer.resolve(users);
  });

  return defer.promise;
}

function getBoostForOtherProducts(user){
  var defer = Q.defer();


  if (!!user){
    findProductsBoughtByUsers([user])
      .then(findUsersWhoBoughtProducts)
      .then(findProductsBoughtByUsers)
      .then(function(products){

        console.log('topProducts: ', JSON.stringify(_.map(products, 'id')));

        defer.resolve(produceBoostFromTerms(products, 'id'));

      });
  }else{
    defer.resolve('');
  }

  return defer.promise;
}

function produceBoostFromTerms(terms){
  console.log('boost by terms: ', terms);
  var boostQuery = '';
  terms.forEach(function(term){
    boostQuery += ' "' + term.id + '"^' + (term.count + 0.1);
  });
  if (boostQuery.length > 0){
    boostQuery = "&bq=id:(" + boostQuery + ")";
  }
  console.log('boostQuery: ', boostQuery);
  //boostQuery = encodeURI(boostQuery);
  return boostQuery;
}